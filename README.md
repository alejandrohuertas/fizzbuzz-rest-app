# Fizz Buzz App

1. prepare and build the app using "mvn clean install"
2. Run the Application by running the main() in class FizzBuzzGameApp
3. Use the postman collection "fizzBuzz.postman_collection.json"and import it into Postman in order to throw the requests to play the game
