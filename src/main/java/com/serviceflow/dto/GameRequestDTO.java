package com.serviceflow.dto;

import java.util.List;

/**
 * Created by AHuertasA on 2/03/2018.
 */
public class GameRequestDTO {

  private List<Integer> numbers;

  private GameRequestDTO(List<Integer> numbers) {
    this.numbers = numbers;
  }

  public GameRequestDTO() {
  }

  public List<Integer> getNumbers() {
    return numbers;
  }

  public static class GameRequestBuilder {

    private List<Integer> numbers;

    public GameRequestBuilder withNumbers(List<Integer> numbers) {
      this.numbers = numbers;
      return this;
    }

    public GameRequestDTO build() {
      return new GameRequestDTO(numbers);
    }
  }
}
