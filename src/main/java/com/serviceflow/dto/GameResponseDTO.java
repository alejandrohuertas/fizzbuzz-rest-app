package com.serviceflow.dto;

import java.util.List;

/**
 * Created by AHuertasA on 2/03/2018.
 */
public class GameResponseDTO {

  private GameResponseDTO(List<String> results) {
    this.results = results;
  }

  public List<String> getResults() {
    return results;
  }

  private final List<String> results;

  public static class GameResponseBuilder {
    List<String> results;

    public GameResponseBuilder withResults(List<String> results) {
      this.results = results;
      return this;
    }

    public GameResponseDTO build() {
      return new GameResponseDTO(results);
    }
  }
}
