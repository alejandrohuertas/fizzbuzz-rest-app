package com.serviceflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by AHuertasA on 2/03/2018.
 */
@SpringBootApplication
class FizzBuzzGameApp extends WebMvcConfigurerAdapter {

  public static void main(String[] args) {
    SpringApplication.run(FizzBuzzGameApp.class, args);
  }

}
