package com.serviceflow.model;

import java.util.List;

/**
 * Created by AHuertasA on 2/03/2018.
 */
public class GameResult {

  private final List<String> results;

  public GameResult(List<String> results) {
    this.results = results;
  }

  public List<String> getResults() {
    return results;
  }

}
