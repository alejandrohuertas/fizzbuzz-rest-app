package com.serviceflow.controller.mapper;

import com.serviceflow.dto.GameResponseDTO;
import com.serviceflow.model.GameResult;

/**
 * Created by AHuertasA on 2/03/2018.
 */
public class GameMapper {
  public static GameResponseDTO makeDTOResponse(GameResult gameResult) {
    return new GameResponseDTO.GameResponseBuilder().withResults(gameResult.getResults()).build();
  }
}
