package com.serviceflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.serviceflow.controller.mapper.GameMapper;
import com.serviceflow.dto.GameRequestDTO;
import com.serviceflow.dto.GameResponseDTO;
import com.serviceflow.service.FizzBuzzService;

import javax.validation.Valid;

/**
 * Created by AHuertasA on 2/03/2018.
 */

@RestController
@RequestMapping("api/fizzBuzz")
public class GameController {


  private FizzBuzzService fizzBuzzService;

  @Autowired
  public GameController(FizzBuzzService fizzBuzzService) {
    this.fizzBuzzService = fizzBuzzService;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<GameResponseDTO> getGameResponse(@Valid @RequestBody GameRequestDTO request){
    return new ResponseEntity<>(GameMapper.makeDTOResponse(fizzBuzzService.getGameResult(request.getNumbers())), HttpStatus.OK);
  }

}
