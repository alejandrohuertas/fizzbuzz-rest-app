package com.serviceflow.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.serviceflow.model.GameResult;

/**
 * Created by AHuertasA on 2/03/2018.
 */
@Service
public class FizzBuzzServiceImpl implements FizzBuzzService {
  @Override
  public GameResult getGameResult(List<Integer> params) {

    return new GameResult(params.stream().map(n -> {
      if (n==0)
        return n.toString();
      if (n % 5 == 0 && n % 3 == 0) {
        return "fizzBuzz";
      } else if (n % 3 == 0) {
        return "fizz";
      } else if (n % 5 == 0) {
        return "buzz";
      }
      return n.toString();
    }).collect(Collectors.toList()));

  }
}
