package com.serviceflow.service;

import java.util.List;

import com.serviceflow.model.GameResult;

/**
 * Created by AHuertasA on 2/03/2018.
 */
public interface FizzBuzzService {

  GameResult getGameResult(List<Integer> params);
}
