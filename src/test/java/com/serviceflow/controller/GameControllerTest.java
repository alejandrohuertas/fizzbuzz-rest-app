package com.serviceflow.controller;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;
import com.serviceflow.controller.mapper.GameMapper;
import com.serviceflow.dto.GameRequestDTO;
import com.serviceflow.dto.GameResponseDTO;
import com.serviceflow.model.GameResult;
import com.serviceflow.service.FizzBuzzService;

/**
 * Created by AHuertasA on 2/03/2018.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class GameControllerTest {

  @Mock
  private FizzBuzzService fizzBuzzService;
  private MockMvc mockMvc;


  @InjectMocks
  private GameController mainController = new GameController(fizzBuzzService);

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
  }

  @Test
  public void givenValidListOfNumbersWhenPlayingThenReturnResults() throws Exception {
    List<Integer> numbers= Lists.newArrayList(1,2,4,6,50);
    GameRequestDTO gameRequest = new GameRequestDTO.GameRequestBuilder().withNumbers(numbers).build();

    List<String> results = Lists.newArrayList("1","2","4" ,"fizz", "buzz");
    GameResult gameResult = new GameResult(results);
    when(fizzBuzzService.getGameResult(gameRequest.getNumbers())).thenReturn(gameResult);

    GameResponseDTO response = GameMapper.makeDTOResponse(gameResult);

    Gson gsonUtil = new Gson();
    String json = gsonUtil.toJson(gameRequest);

    String jsonResponse = gsonUtil.toJson(response);
    mockMvc.perform(post("/api/fizzBuzz").accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(json))
        .andExpect(status().isOk()).andExpect(content().string(containsString(jsonResponse)));
  }

  //TODO validate when the input is not well structured

  @SuppressWarnings("unchecked")
  @Test
  public void givenInvalidListWhenPlayingThenThrowException() throws Exception {
    List numbers= Lists.newArrayList(1,2,4,6,50,"A String");
    GameRequestDTO gameRequest = new GameRequestDTO.GameRequestBuilder().withNumbers(numbers).build();

    List<String> results = Lists.newArrayList("1","2","4" ,"fizz", "buzz");
    GameResult gameResult = new GameResult(results);
    when(fizzBuzzService.getGameResult(gameRequest.getNumbers())).thenReturn(gameResult);

    GameResponseDTO response = GameMapper.makeDTOResponse(gameResult);

    Gson gsonUtil = new Gson();
    String json = gsonUtil.toJson(gameRequest);

    mockMvc.perform(post("/api/fizzBuzz").accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .content(json))
        .andExpect(status().isBadRequest());
  }
}
