package com.serviceflow;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.serviceflow.controller.GameController;

/**
 * Created by AHuertasA on 2/03/2018.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class FizzBuzzGameAppTest {

  @Autowired
  private GameController controller;

  @Test
  public void contextLoads() throws Exception {
    assertThat(controller).isNotNull();
  }

}