package com.serviceflow.service;

import com.serviceflow.model.GameResult;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by AHuertasA on 2/03/2018.
 */
public class FizzBuzzServiceTest {

  @Test
  public void givenAValidNumberWhenPlayingThenReturn (){
    FizzBuzzService fizzBuzzService = new FizzBuzzServiceImpl();

    List<String> expectedResults = Lists.newArrayList("fizz");
    GameResult gameResult = fizzBuzzService.getGameResult(Lists.newArrayList(27));

    assertArrayEquals(expectedResults.toArray(), gameResult.getResults().toArray());

  }


  @Test
  public void givenAValidNumbersNotFizzOrBuzzWhenPlayingThenReturn (){
    FizzBuzzService fizzBuzzService = new FizzBuzzServiceImpl();

    List<String> expectedResults = Lists.newArrayList("1", "4","17","26","71","0");
    GameResult gameResult = fizzBuzzService.getGameResult(Lists.newArrayList(1,4,17,26,71,0));

    assertArrayEquals(expectedResults.toArray(), gameResult.getResults().toArray());

  }


  @Test
  public void givenNoNumbersNotFizzOrBuzzWhenPlayingThenReturnEmptyGameResult (){
    FizzBuzzService fizzBuzzService = new FizzBuzzServiceImpl();
    GameResult gameResult = fizzBuzzService.getGameResult(Lists.newArrayList());
    assertTrue( gameResult.getResults().isEmpty());
  }

  @Test
  public void givenAValidNumbersAllFizzOrBuzzWhenPlayingThenReturn (){
    FizzBuzzService fizzBuzzService = new FizzBuzzServiceImpl();

    List<String> expectedResults = Lists.newArrayList("fizz", "buzz","fizzBuzz","fizz","fizz");
    GameResult gameResult = fizzBuzzService.getGameResult(Lists.newArrayList(3, 5,15,27,72));

    assertArrayEquals(expectedResults.toArray(), gameResult.getResults().toArray());

  }

  @Test
  public void givenAValidNumbersAllFizzBuzzWhenPlayingThenReturn (){
    FizzBuzzService fizzBuzzService = new FizzBuzzServiceImpl();

    List<String> expectedResults = Lists.newArrayList("fizzBuzz", "fizzBuzz","fizzBuzz","fizzBuzz","fizzBuzz");
    GameResult gameResult = fizzBuzzService.getGameResult(Lists.newArrayList(75, 135,15,105,45));

    assertArrayEquals(expectedResults.toArray(), gameResult.getResults().toArray());

  }



}